---
title: Social media con Mastodon
---

Fai dei "twit" con social media liberi
{: .slogan-subtitle }

Mastodon è un software gratuito che permette di creare e mantenere un'istanza di microblogging simile a Twitter. A differenza di quest'ultimo, Mastodon è dispiegato su una rete federata di istanze indipendenti l'una dall'altra, ma in grado di comunicare tra loro. Questo permette quindi alle utilizzatrici/tori di comunicare tra loro indipendentemente dall'istanza su cui essi ha un *account*.

Ci sono molti forum aperti al pubblico e gestiti da comunità che operano secondo regole etiche e rispettose dei dati delle/degli utenti. Potete aderire a quello di vostra scelta senza bisogno di installarne uno, soprattutto se non si vuole assumere le (a volte pesanti) responsabilità. È possibile avvicinarsi a un associazione di cui vi fidate e che ha una tale istanza. Alcuni CHATONS offrono queste opportunità (vedi [chatons.org](http://chatons.org/)).
{: .slogan-contenu }

## Partecipare al Fediverso

Un esempio di questo è l'email: se avete un account su @fornitore1, è possibile inviare un messaggio ad un persona che ha un account presso @fornitore2. Sullo stesso principio, la rete Mastodon in cui le istanze e i servizi comunicano è una rete acentrica e distribuita (decentralizzazione). Lo standard che permette loro di comunicare si chiama ActivityPub. Viene utilizzato anche per altri tipi di supporti come Peertube (hosting video decentralizzato), Plume (hosting di blog decentralizzato), Pleroma (un altro microblogging), ecc. Grazie all'uso di questo unico protocollo di comunicazione (in altre parole, una lingua), tutti questi i media possono comunicare tra loro (per esempio, è possibile commentare un video sulla rete Peertube con il tuo account da un'istanza Mastodon). 

Tutte queste istanze e le relazioni tra tali servizi sono strutturate sulla base del cosiddetto Fediverso (parola composta da "Federazione" e "Universo") la cui caratteristica principale è di essere costruito sulla base di software libero e di raggruppare istanze che hanno regole proprie (se un servizio ha lo scopo di fare pubblicità, le/gli utenti sono libere/i di accettarlo o meno). 

Mastodon permette di:

- creare un'istanza di microblogging e aprire le iscrizioni a tutte/i o a nessuno (a parte l’amministratrice/tore dell’istanza in questione),
- decidere con quali altri organismi (quale parte della rete federata), un servizio può comunicare e consentire alle/agli utenti di comunicare tra loro da un servizio all'altro.

Per un'organizzazione è possibile creare un'istanza che può essere utilizzata solo internamente, o aprirla limitando le iscrizioni solo ai suoi membri o semplicemente fare un'istanza con un unico account, ma comunicante con il resto della rete per trasmettere le notizie. Possiamo seguire l'esempio della DINSIC in Francia (Direction interministérielle du numérique), che nel 2017 ha aperto un'istanza dedicata alle/ai dipendenti⋅pubblici⋅dello Stato francese federata con altri servizi.
{: .encart }

## Cosa facciamo con Mastodon?

...facciamo «strombazzamenti», o «toots» in inglese. In sintesi, pubblichiamo dei contenuti: dei testi  limitati il più delle volte a 500 caratteri (che non è male), immagini, video, suoni... È possibile rendere i «toots» visibili al pubblico, limitarli solo a coloro che ti seguono, o ai soli membri del servizio a cui siete abbonati. È inoltre possibile inviare messaggi privati. È possibile creare un profilo pubblico, segnalare alcuni dei toot mettendoli tra i preferiti, per non parlare delle caratteristiche offerte dai software che permettono di connettersi a Mastodon.

## Comunicare internamente

"Tutti hanno un account Facebook", questo è l'argomento spesso citato quando viene chiesto come comunicano i membri di un'organizzazione, sia che i membri siano volontari o professioniste/i. Questo implica una confusione tra il profilo personale di un/un'utente e il suo lavoro (anche come volontaria/o) e significa anche che per alcune/i si deve aprire un proprio account che espone i propri dati personali alle condizioni di Facebook, indipendentemente dalle loro convinzioni. Un social media non dovrebbe essere usato per comunicare internamente... tranne che in il caso di un'istanza Mastodon chiusa al pubblico e ad altri casi (in questo caso, si potrebbe anche utilizzare un vero e proprio strumento di lavoro di squadra come [Mattermost](mattermost.md)).

## Per comunicare con il pubblico

Dal 2017, il numero totale di utenti di Mastodon è in rapida crescita. Tuttavia, per quanto si dovrebbe incoraggiare il vostro organizzazione ad utilizzarlo, tanto quanto non è necessario limitare l'uso della comunicazione della vostra organizzazione a questo mezzo. Se il vostro target pubblico è su media centralizzati come Twitter o Facebook, nulla vi impedisce di creare degli account e di comunicare notizie, invitando il pubblico a venire invece sul vostro sito web per scaricare contenuti e venire su Mastodon per aver a che fare con voi. La differenza è che le persone che desiderano unirsi a voi non saranno obbligate accettare le discutibili condizioni d'uso di Twitter, oppure Facebook. Questa è anche un'opportunità per creare la propria comunità di utilizzatrici/tori.

## Moderazione

Oltre alla necessità di mantenere tecnicamente un'istanza di Mastodon per le/gli utenti⋅esterne/i⋅alla vostra organizzazione si deve anche realizzare un vero e proprio lavoro di moderazione, nell'ambito della norme applicabili ai gestori. È più facile (anche se vincolante) moderare  gli account appartenenti a persone che conoscete. In un primo tempo è quindi prudente aprire un'istanza solo per i vostri membri e allo stesso tempo collegarla con le altre.

## Le applicazioni

Oltre all'interfaccia web di Mastodon (che può essere nei colori dell'host dell'istanza), è possibile installare sul proprio smartphone il software Fedilab o Tusky per limitarsi ai più famosi. Si chiamano software "client". In futuro, tali software si moltiplicheranno e alcuni di essi vinceranno il premio del numero di utilizzatrici/tori: sta a voi scegliere come connettervi!
