---
title: Média social avec Mastodon
---

Faites des « pouets » avec un média social libre
{: .slogan-subtitle }

Mastodon est un logiciel libre permettant de créer et maintenir une
instance de microblogging à l'image de Twitter. À la différence de ce
dernier, Mastodon se déploie sur un réseau fédéré d'instances
indépendantes les unes des autres mais qui peuvent communiquer entre
elles. Cela permet donc aux utilisateur⋅rice⋅s de communiquer entre
elles⋅eux quelle que soit l'instance sur laquelle elles⋅ils ont un
compte.

Il existe de nombreuses instances ouvertes au public et maintenues par
des communautés actives selon des règles éthiques et respectueuses des
données des utilisateur⋅rice⋅s. Vous pouvez rejoindre celle de votre
choix sans avoir besoin d'en installer une, surtout si vous ne désirez
pas en assumer les (parfois lourdes) responsabilités. Vous pouvez vous
rapprocher d'une association en qui vous avez confiance et qui possède
une telle instance. Certains CHATONS offrent ces opportunités (voir sur
[chatons.org](http://chatons.org/)).
{: .slogan-contenu }

## Participer au Fédiverse

On peut prendre l'exemple du courrier électronique : si vous avez un
compte chez @founisseur1, vous pouvez envoyer un message à une personne
qui possède un compte chez @fournisseur2. Sur le même principe, le
réseau où communiquent les instances et les comptes Mastodon est un
réseau acentré et distribué (décentralisation).
Le standard qui leur permet de communiquer entre
elles se nomme ActivityPub. Il est aussi utilisé pour d'autre types de
médias sociaux tels Peertube (hébergement décentralisé de vidéos), Plume
(hébergement décentralisé de blogs), Pleroma (un autre système de
microblogging), etc. Grâce à l'utilisation de cet unique protocole de
communication (autrement dit, un langage), tous ces médias peuvent
communiquer entre eux (vous pouvez commenter une vidéo sur le réseau
Peertube avec votre compte depuis une instance Mastodon, par exemple).

Toutes ces instances et relations entre de tels services se structurent
sur ce qu'il est convenu d'appeler le Fédiverse (mot-valise composé de
« fédération » et « univers ») dont la caractéristique principale est
d'être construit sur la base de logiciels libres et de regrouper des
instances qui ont leurs propres règles (si une instance a pour objectif
de faire de la publicité, libre aux utilisateur⋅rice⋅s de s'y
rendre\... ou pas).

Mastodon permet :

-   de créer une instance de microblogging et d'ouvrir les inscriptions
    à tou⋅te⋅s, à quelques-un⋅e⋅s ou à personne (à part
    l'administrateur⋅rice de l'instance en question),

-   de décider avec quelles autres instances (quelle partie du réseau
    fédéré) une instance peut communiquer et permettre aux
    utilisateur⋅rice⋅s de communiquer entre elles⋅eux d'une instance à
    l'autre.

Pour une organisation, il est possible de créer une instance utilisable
seulement en interne, ou de l'ouvrir tout en restreignant les
inscriptions uniquement à ses membres, ou d'en faire simplement une
instance avec un seul compte mais communiquant avec tout le reste du
réseau pour diffuser des nouvelles. On peut suivre en France l'exemple
de la DINSIC (Direction interministérielle du numérique), qui a ouvert
en 2017 une instance réservée aux agent⋅e⋅s public⋅que⋅s de l'État
français et néanmoins fédérée avec les autres instances.
{: .encart }

## Que fait-on sur Mastodon ?

... on fait des « pouets », ou des « toots » (en anglais). Pour faire
simple, on poste des contenus (des textes limités la plupart du temps à
500 caractères (ce qui n'est pas si mal), des images, des vidéos, des
sons... Il est possible de rendre ses pouets visibles publiquement, de
les restreindre uniquement à celles et ceux qui vous suivent, ou aux
seul⋅e⋅s membres de l'instance sur laquelle vous êtes. Vous pouvez aussi
envoyer des messages privés. Vous pouvez créer un profil public, marquer
certains pouets ou en mettre en favoris, sans compter les
fonctionnalités offertes par les logiciels qui vous permettent de vous
connecter à Mastodon.

## Communiquer en interne

« Tout le monde a un compte Facebook », tel est l'argument souvent
énoncé lorsqu'on demande comment communiquent les membres d'une
organisation, qu'elles⋅ils soient bénévoles ou professionnel⋅le⋅s. Cela
implique une confusion entre le profil personnel d'un⋅e
utilisateur⋅rice et son travail (même bénévole) et cela implique aussi
pour certain⋅e⋅s de devoir ouvrir un compte qui expose leurs données
personnelles aux conditions de Facebook, quelles que soient leurs
convictions. Un média social ne devrait pas être utilisé pour
communiquer en interne\... sauf dans le cas d'une instance Mastodon
fermée au public et aux autres instances (dans ce cas, autant utiliser
un véritable outil de travail d'équipe tel que [Mattermost](mattermost.md)).

## Pour communiquer avec le public

Depuis 2017, le nombre total de personnes utilisatrices de Mastodon est
en forte croissance. Néanmoins, autant vous devriez encourager les
membres de votre organisation à l'utiliser, autant il est facultatif de
restreindre la communication de votre organisation à ce média. Si votre
public cible se situe sur les médias centralisés tels Twitter ou
Facebook, rien ne vous empêche d'y créer des comptes et d'y
communiquer des nouvelles, tout en invitant votre audience à venir
plutôt sur votre site internet pour télécharger des contenus et à venir
sur Mastodon pour échanger avec vous. La différence, c'est que les
personnes qui souhaitent vous rejoindre ne seront pas obligées
d'accepter les conditions d'utilisation discutables de Twitter ou
Facebook. Par ailleurs, c'est l'occasion de créer votre propre
communauté d'utilisateur⋅rice⋅s.

## Modération

Outre la nécessité de maintenir techniquement l'instance que l'on
souhaite mettre en place, ouvrir une instance Mastodon à des
utilisateur⋅rice⋅s extérieur⋅e⋅s à votre organisation suppose de
réaliser un vrai travail de modération, au titre des règlements
applicables aux hébergeurs. Il est plus facile (bien que contraignant)
de modérer des comptes appartenant à des personnes que vous connaissez.
Dans un premier temps, il est donc prudent d'ouvrir une instance
uniquement pour vos membres tout en la fédérant avec les autres.

## Les applications

Outre l'interface web de Mastodon (qui pourra être aux couleurs de
l'hébergeur de l'instance), vous pouvez installer sur votre smartphone
les logiciels Fedilab ou Tusky pour les plus connus. On les appelle des
logiciels « clients ». À l'avenir, de tels logiciels vont se multiplier
et certains remporteront la palme du nombre d'utilisateur⋅rice⋅s : à
vous de choisir comment vous connecter !
