---
title: Administrez votre association avec Galette
---

Un outil de gestion d'adhérents et de cotisations en ligne
{: .slogan-subtitle }

Pour répondre aux besoins des petites associations, Galette
(Gestionnaire d'Adhérents en Ligne Extrêmement Tarabiscoté mais
Tellement Efficace) répond à des besoins concrets et repose sur
l'expérience des utilisateur⋅rice⋅s. Ce logiciel fonctionne comme un
service en ligne et permet une gestion collective : fiches adhérent⋅e⋅s,
mailing, échéancier, cotisations.

## Une installation facile

Vous pouvez installer Galette sur une machine locale (pensez à faire des
sauvegardes régulières), mais l'intérêt de Galette est surtout de
fonctionner en ligne pour favoriser le travail collaboratif.

Il va de soi que la facilité est toujours relative aux compétences
disponibles. Cependant, si dans votre association vous avez un⋅e membre
qui dispose déjà de son propre blog ou qui a déjà installé un service
sur un serveur distant, l'installation de Galette ne devrait pas poser
de problème. Par rapport au gain escompté en temps et en facilité de
gestion, il peut être intéressant pour votre association de prendre le
temps de trouver une personne compétente qui pourra se charger (sur la
durée !) de l'instance Galette dédiée à votre organisation.

Avant -- La gestion des adhésions était assurée par la fonction de
trésorerie de l'association, qui utilisait un tableur sur un ordinateur
personnel. Les rappels, les mises à jours représentaient un temps de
bénévolat non disponible pour d'autres projets. Les dons, les cotisation
et les reçus étaient comptabilisés et traités un à un.<br>
Maintenant -- Avec Galette la gestion peut se faire de manière plus
collective. Les courriels de rappels sont envoyés automatiquement, il
est possible d'obtenir un état de trésorerie en quelques clics, ainsi
que les fiches des membres. La gestion n'est plus assurée par une seule
personne cantonnée dans ce rôle.
{: .encart }

## Avantages

Le premier avantage de Galette, c'est justement l'auto-hébergement, ce
qui permet aux membres d'un conseil d'administration d'une association
de travailler ensemble à la gestion de cette dernière. Cela permet aussi
à tou⋅te⋅s les membres de se connecter, payer leurs cotisation, mettre à
jour eux⋅elles-mêmes leurs informations personnelles, interagir grâce à
un service de mailing, de prêt (de livres, d'objets divers), de gestion
d'événements (plusieurs extensions sont disponibles, plus ou moins
spécialisées).

## Un logiciel conçu par des utilisateur⋅rice⋅s pour des utilisateur⋅rice⋅s

Galette est une illustration pertinente de ce qu'est un logiciel libre.
Il fut au départ développé par une association lyonnaise
d'utilisateur⋅rice⋅s de logiciels libres qui ne trouvait pas de solution
pour gérer les adhésions. Le développement de Galette s'est poursuivi en
proposant le logiciel à toutes les associations qui en éprouvaient le
besoin et en valorisant les retours d'expérience qui permettent
d'améliorer sans cesse ce logiciel.

Comme le code est non seulement disponible mais aussi éprouvé, Galette
peut être utilisé en toute confiance.
{: .slogan-contenu }

Si vous utilisez Galette, faites-le savoir auprès des développeur⋅se⋅s
([sur le site officiel](https://galette.eu/site/fr/)) et contribuez vous-même, ne serait-ce qu'en
communiquant vos retours d'expérience.
