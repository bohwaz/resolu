---
title: Décentralisation
---

Un enjeu pour toute l'Économie Sociale et Solidaire
{: .slogan-subtitle }

## Un peu de théorie

Pour comprendre les enjeux de la décentralisation, il faut distinguer
Internet et le web. Le premier est une interconnexion de réseaux
(réseaux gouvernementaux, réseaux privés, réseaux publics, etc.) et
repose à la fois sur des infrastructures techniques et sur l'usage de
protocoles permettant à des informations d'être échangées entre
machines et entre réseaux. Le second est autrement appelé la « toile
mondiale » (world wide web). C'est l'une des couches applicatives
d'Internet permettant à des contenus d'être lisibles tout en étant
liés entre eux, en particulier grâce au protocole HTML (ce que vous
voyez la plupart du temps en « surfant sur la toile »).

Dans un monde numérique idéal, il n'est nulle part question de
centralisation de l'information ou de concentration des ressources
techniques : tout devrait fonctionner de manière autonome, c'est-à-dire
indépendamment d'un opérateur sans lequel les échanges seraient
impossibles.
{: .slogan-contenu }

Normalement, avec Internet, si un des nœuds du réseau fait défaut, les
paquets d'informations peuvent toujours circuler : ils passent par
d'autres chemins maintenus par une pluralité d'acteurs.

## Centraliser, concentrer

On parle de centralisation ou de concentration dès lors qu'un de ces
acteurs rend obligatoire le passage des contenus informatifs par des
protocoles ou des formats que lui seul connaît ou autorise
(centralisation forcée de l'information) ou par ses machines et systèmes
techniques  à lui (concentration des ressources). Cela peut être par
exemple un gouvernement répressif qui concentre les communications de
son réseau national de manière à les censurer. Mais ce peut être aussi
un opérateur commercial qui cherche à garder captif⋅ve⋅s ses
utilisateur⋅rice⋅s, afin de leur distribuer de la publicité et pour
récolter leurs données personnelles. C'est l'exemple typique de
Facebook qui ne permet pas à ses utilisateur⋅rice⋅s de communiquer avec
d'autres utilisateur⋅rice⋅s non membres sans utiliser les outils de
Facebook. Mais c'est aussi l'exemple de Google/Alphabet et son service
de vidéos Youtube : certes vous pouvez toujours visionner des vidéos
sans vous inscrire mais à cause de la place prépondérante qu'a pris
Youtube dans le monde, il est devenu presque impossible à d'autres
organisations de proposer un tel service, à la fois pour des raisons
financières mais aussi parce que Google/Alphabet a acquis une situation
hégémonique.

Pour maîtriser, contrôler et surveiller toujours davantage la
circulation des informations sur le web et renforcer leur modèle
économique, les acteurs hégémoniques ont fini par concentrer aussi une
grande part d'Internet en imposant leurs propres infrastructures
techniques (fermes de serveurs, services d'hébergement, et même
l'acquisition de câbles sous-marins reliant les continents).

## La neutralité

L'un des principaux leviers permettant de faire face à ce qui peut
s'apparenter à un hold-up sur l'information mondiale, c'est le
concept de neutralité du réseau. C'est à la fois un principe et un
outil juridique : la neutralité du réseau implique qu'un message doit
circuler de la même manière indépendamment de son contenu, de l'émetteur
ou récepteur. Que diriez-vous si une dépêche journalistique venait à
être systématiquement ralentie ou modifiée selon la tendance politique
du site d'information qui la diffuse ou selon à qui appartiennent les
« tuyaux » via lesquels elle transite ? Ou plus simplement, laisseriez
vous votre facteur⋅rice ouvrir votre courrier et trier lui⋅elle même,
selon le contenu, entre ce qu'il⋅elle vous remettra dans la journée et
ce que vous ne recevrez que plusieurs jours après ?

Telles sont les raisons qui poussent aujourd'hui de nombreuses
organisations à plaider pour une (re-)décentralisation d'Internet,
autrement dit mettre fin aux pratiques hégémoniques des grandes
entreprises mondiales des services numériques et favoriser l'usage de
protocoles ouverts. C'est aussi l'occasion pour de nouvelles formes
d'économie de voir le jour, basées en particulier sur  les échanges de
pair-à-pair. Mais attention, de nouveaux modèles d'affaire échappant à
toute régulation (notamment lorsqu'ils utilisent des technologies de
type blockchain) peuvent aussi bien constituer une menace pour les
libertés en concentrant à leur manière les compétences et les finances.
Si le logiciel libre trouve sa force dans la pluralité des initiatives,
[l'éthique](../collaborer/ethique.md) n'en est pas moins importante.

## La décentralisation d'Internet est un enjeu de l'ESS

Dans ce contexte, l'Économie Sociale et Solidaire parle peu ou prou le
même langage. En effet la gouvernance classique des entreprises et des
institutions est généralement pyramidale, avec une forme de
centralisation de l'information pour des besoins de contrôle. Dans un
modèle coopératif, au contraire, la solidité de la structure dépend de
plusieurs niveaux démocratiques de régulation, ce qui aboutit à une
décentralisation des instances décisionnelles. Cela traduit un besoin
très fort d'autonomie numérique. En effet, il serait parfaitement
contre-productif de recentraliser la décision en utilisant les services
Internet d'une seule entreprise hégémonique : cela revient à mettre en
danger les données de toutes les personnes utilisatrices, rendre les
processus décisionnels dépendants d'un acteur tiers dont la confiance
et la fiabilité peuvent être mises en doute, et cela remet aussi en
cause l'autonomie numérique de l'organisation qui les utilise.

Les valeurs de l'ESS, en particulier dans le monde associatif, vont
presque naturellement à l'encontre des pratiques numériques marchandes.
Même s'il n'est pas toujours facile et souvent coûteux de mettre en
place des systèmes d'information et de communication autonomes et basés
sur des logiciels libres, il est de plus en plus indispensable de le
faire. On peut suivre pour cela l'exemple des CEMÉA, avec le projet
[Zourit](../collaborer/zourit.md).
{: .encart }

Les compétences n'étant pas toujours disponibles, le grand principe de
solidarité doit alors jouer à plein : mutualiser ces compétences entre
organisations de l'ESS, faire appel à des acteurs éthiques et
solidaires faisant eux-mêmes partie de l'ESS, évaluer la confiance dans
ces acteurs éthiques, essaimer les pratiques de manière à favoriser la
décentralisation d'Internet et l'autonomie de tou⋅te⋅s.
